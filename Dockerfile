FROM centos/s2i-base-centos7

MAINTAINER Josh Fritts <jfritts-consultant@scholastic.com>

EXPOSE 8080

RUN set -ex && \
  for key in \
    94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
    FD3A5288F042B6850C66B31F09FE44734EB7990E \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
    DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 \
    56730D5401028683275BD23C23EFEFE93C4CFFFE \
  ; do \
    gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
  done && \
  yum install -y epel-release && \
  INSTALL_PKGS="bzip2 nss_wrapper nginx python python2-pip" && \
  yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
  rpm -V $INSTALL_PKGS && \
  yum clean all -y

RUN chown -R 1001:0 /opt/app-root

# Install uWSGI
RUN pip install uwsgi

# Standard set up Nginx
ENV NGINX_VERSION 1.9.11-1~jessie

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/

# Install Supervisord
RUN yum install -y supervisor
COPY supervisord.conf /etc/supervisord.conf

# Copy the entrypoint that will generate Nginx additional configs

COPY ./s2i/bin/ $STI_SCRIPTS_PATH

RUN chown -R 1001:0 /var/log/
RUN chown -R 1001:0 /var/lib/nginx
RUN chmod 777 /run
RUN chmod 777 /etc/supervisord.conf
COPY uwsgi.ini /etc/uwsgi/
RUN mkdir -p /opt/app-root/src/secrets
COPY openshift-tokens /opt/app-root/src/secrets/
COPY container-hub /opt/app-root/src/secrets/

RUN yum install -y python-devel openldap-devel

USER 1001

CMD $STI_SCRIPTS_PATH/usage
